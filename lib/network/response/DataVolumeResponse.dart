
import 'package:pertamini/ancestor/BaseResponse.dart';

class DataVolumeResponse extends BaseResponse{
  VolumeData data;

  DataVolumeResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = VolumeData.fromJson(json["data"]);
  }
}

class VolumeData {
  int id;
  String rfid;
  String kode;
  String nama;
  String jenisUsaha;
  int provinsiId;
  int kotaId;
  int kecamatanId;
  String alamat;
  String handphone;
  String epay;
  List<LayananData> layanan;

  VolumeData.fromJson(Map<String, dynamic> json){
    id = json["id"] ?? 0;
    rfid = json["rfid"] ?? "";
    kode = json["kode"] ?? "";
    nama = json["nama"] ?? "";
    jenisUsaha = json["jenis_usaha"] ?? "";
    provinsiId = json["provinsi_id"] ?? 0;
    kotaId = json["kota_id"] ?? 0;
    kecamatanId = json["kecamatan_id"] ?? 0;
    alamat = json["alamt"] ?? "";
    handphone = json["handphone"] ?? "";
    epay = json["epay"] ?? "";

    layanan = List();
    json["layanan"].forEach((it) => layanan.add(LayananData.fromJson(it)));
  }
}

class LayananData {
  int id;
  String deviceId;
  int pengecerId;
  String layananId;
  String kode;
  String keterangan;
  String slug;
  String kapasitas;
  String jenisTangki;
  String panjang;
  String lebar;
  String tinggi;
  String diameter;
  String volumeTangki;
  String updatedTangki;
  String layanan;

  LayananData.fromJson(Map<String, dynamic> json){
    id = json["id"] ?? 0;
    deviceId = json["device_id"] ?? "";
    pengecerId = json["pengecer_id"] ?? 0;
    layananId = json["layanan_id"] ?? "";
    kode = json["kode"] ?? "";
    keterangan = json["keterangan"] ?? "";
    slug = json["slug"] ?? "";
    kapasitas = json["kapasitas"] ?? "";
    jenisTangki = json["jenis_tangki"] ?? "";
    panjang = json["panjang"] ?? "";
    lebar = json["lebar"] ?? "";
    tinggi = json["tinggi"] ?? "";
    diameter = json["diameter"] ?? "";
    volumeTangki = json["volume_tangki"] ?? "0";
    updatedTangki = json["updated_tangki"] ?? "";
    layanan = json["layanan"] ?? "";
  }
}