import 'package:pertamini/ancestor/BaseResponse.dart';

class DataPenjualanResponse extends BaseResponse {
  List<PenjualanData> data;

  DataPenjualanResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();
    if(json['data'] != null) {
      json['data'].forEach((it) => data.add(PenjualanData.fromJson(it)));
    }
  }
}

class PenjualanData {
  int id;
  String tanggal;
  String kode;
  int pengecerId;
  String pengecerNama;
  String pengecerRfid;
  String totalLiter;
  String totalBayar;
  String metodePembayaran;
  String epay;
  String cash;
  String status;
  List<PenjualanDetail> detail;

    PenjualanData.fromJson(Map<String, dynamic> json){
      id = json["id"] ?? 0;
      tanggal = json["tanggal"] ?? "";
      kode = json["kode"] ?? "";
      pengecerId = json["pengecer_id"] ?? 0;
      pengecerNama = json["pengecer_nama"] ?? "";
      pengecerRfid = json["pengecer_rfid"] ?? "";
      totalLiter = json["total_liter"] ?? "";
      totalBayar = json["total_bayar"] ?? "";
      metodePembayaran = json["metode_pembayaran"] ?? "";
      epay = json["epay"] ?? "";
      cash = json["cash"] ?? "";
      status = json["status"] ?? "";

      detail = List();
      json["detail"].forEach((it) => detail.add(PenjualanDetail.fromJson(it)));
    }
}

class PenjualanDetail {
  int id;
  int penjualanId;
  String penjualanKode;
  int layananId;
  String kode;
  String keterangan;
  String slug;
  String liter;
  String hargaLiter;
  String bayar;

  PenjualanDetail.fromJson(Map<String, dynamic> json){
    id = json["id"] ?? 0;
    penjualanId = json["penjualan_id"] ?? 0;
    penjualanKode = json["penjualan_kode"] ?? "";
    layananId = json["layanan_id"] ?? 0;
    kode = json["kode"] ?? "";
    keterangan = json["keterangan"] ?? "";
    slug = json["slug"] ?? "";
    liter = json["liter"] ?? "";
    hargaLiter = json["harga_liter"] ?? "";
    bayar = json["bayar"] ?? "";
  }
}