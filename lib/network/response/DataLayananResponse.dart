import 'package:pertamini/ancestor/BaseResponse.dart';

class DataLayananResponse extends BaseResponse {
  List<DataLayanan> data;

  DataLayananResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = List();

    json["data"].forEach((it) => data.add(DataLayanan.fromJson(it)));
  }
}

// class DataLayanan {
//   int id;
//   String device_id;
//   String layanan;
//   String volume_tangki;

//   DataLayanan.fromJson(Map<String, dynamic> json){
//     id = json['id'] ?? 0;
//     device_id = json['device_id'] ?? '';
//     layanan = json['layanan'] ?? '';
//     volume_tangki = json['volume_tangki'] ?? '';
//   }
// }

class DataLayanan {
  int id;
  String kode;
  String keterangan;
  String slug;
  int hargaLiter;

  double liter;
  int bayar;

  DataLayanan.fromJson(Map<String, dynamic> json) {
    id = json["id"] ?? 0;
    kode = json["kode"] ?? "";
    keterangan = json["keterangan"] ?? "";
    slug = json["slug"] ?? "";
    hargaLiter = json["harga_liter"] != null ? double.parse(json["harga_liter"]).toInt() : 0;
  }

  @override
  bool operator ==(other) => id == other.id;

  @override
  int get hashCode => id;

}