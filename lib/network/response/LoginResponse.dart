import 'dart:convert';

import 'package:pertamini/ancestor/BaseResponse.dart';

class LoginResponse extends BaseResponse {
  UserData data;
  
  LoginResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = UserData.fromJson(json["data"]);
  }
}

class UserData {
  int id;
  String rfid;
  String kode;
  String nama;
  String jenisUsaha;
  int provinsiId;
  int kotaId;
  int kecamatanId;
  String alamat;
  String handphone;
  String epay;

  UserData.fromJson(Map<String, dynamic> json){
    id = json["id"] ?? 0;
    rfid = json["rfid"] ?? "";
    kode = json["kode"] ?? "";
    nama = json["nama"] ?? "";
    jenisUsaha = json["json_usaha"] ?? "";
    provinsiId = json["provinsi_id"] ?? 0;
    kotaId = json["kota_id"] ?? 0;
    kecamatanId = json["kecamatan_id"] ?? 0;
    alamat = json["alamat"] ?? "";
    handphone = json["handphone"] ?? "";
    epay = json["epay"] ?? "0";
  }

  String toJson() => jsonEncode({
    "id" : id,
    "rfid" : rfid,
    "kode" : kode,
    "nama" : nama,
    "jenis_usaha" : jenisUsaha,
    "provinsi_id" : provinsiId,
    "kota_id" : kotaId,
    "kecamatan_id" : kecamatanId,
    "alamat" : alamat,
    "handphone" : handphone,
    "epay" : epay,
  });
}