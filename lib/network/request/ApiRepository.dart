import 'dart:convert';

import 'package:pertamini/ancestor/BaseRepository.dart';
import 'package:pertamini/ancestor/BaseResponse.dart';
import 'package:pertamini/ancestor/BaseState.dart';
import 'package:pertamini/network/response/DataLayananResponse.dart';
import 'package:pertamini/network/response/DataPenjualanResponse.dart';
import 'package:pertamini/network/response/DataVolumeResponse.dart';
import 'package:pertamini/network/response/LoginResponse.dart';

class ApiRepository extends BaseRepository {
  ApiRepository(BaseState baseState) : super(baseState, baseUrl: "http://mdthosting.com/pertamini/public/api/");

  void executeLogin(int typeRequest, Map<String, dynamic> params, Function(LoginResponse) completion) async {
    var response = await post("pengecer/login", params, typeRequest);

    if(response != null){
      completion(LoginResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetVolume(int typeRequest, String id, Function(DataVolumeResponse) completion) async {
    var response = await get("sensor/data/$id", null, typeRequest);

    if(response != null){
      completion(DataVolumeResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetLayanan(int typeRequest, int id, Function(DataLayananResponse) completion) async {
    var response = await get("layanan/user/$id", null, typeRequest);

    if(response != null){
      completion(DataLayananResponse.fromJson(jsonDecode(response.data)));
    }
  }

  // void executeGetLayanan(int typeRequest, Function(DataLayananResponse) completion) async {
  //   var response = await get("layanan", null, typeRequest);

  //   if(response != null){
  //     completion(DataLayananResponse.fromJson(jsonDecode(response.data)));
  //   }
  // }

  void executeGetPenjualan(int typeRequest, String id, Function(DataPenjualanResponse) completion) async {
    var response = await get("penjualan/data/$id", null, typeRequest);

    if(response != null){
      completion(DataPenjualanResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeBooking(int typeRequest, Map<String, dynamic> params, Function(BaseResponse) completion) async {
    var response = await post("penjualan/simpan", params, typeRequest);

    if(response != null){
      completion(BaseResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeTopup(int typeRequest, Map<String, dynamic> params, Function(BaseResponse) completion) async {
    var response = await post("pengecer/topup", params, typeRequest);

    if(response != null){
      completion(BaseResponse.fromJson(jsonDecode(response.data)));
    }
  }

}