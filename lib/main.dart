import 'package:flutter/material.dart';
import 'package:pertamini/ui/booking/BookingUI.dart';
import 'package:pertamini/ui/home/HomeUI.dart';
import 'package:pertamini/ui/login/LoginUI.dart';
import 'package:pertamini/ui/splash_screen/SplashScreen.dart';
import 'package:pertamini/ui/transaksi/TransaksiUI.dart';
import 'package:pertamini/utils/ColorUtils.dart';

void main() => runApp(MyApp());

//export PATH="$PATH:/Users/apple/Documents/flutter/bin"

class MyApp extends StatelessWidget {
  static const ROUTE_SPLASH_SCREEN = "/";
  static const ROUTE_LOGIN = "/login";
  static const ROUTE_HOME = "/home";
  static const ROUTE_BOOKING = "/booking";
  static const ROUTE_TRANSAKSI = "/transaksi";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pertamini',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: ColorUtils.bgEpayGradientStart,
      ),
      onGenerateRoute: (settings){
        switch(settings.name){
          case ROUTE_SPLASH_SCREEN:
            return MaterialPageRoute(builder: (_) => SplashScreenUI());
          case ROUTE_LOGIN:
            return MaterialPageRoute(builder: (_) => LoginUI());
          case ROUTE_HOME:
            return MaterialPageRoute(builder: (_) => HomeUI());
          case ROUTE_BOOKING:
            return MaterialPageRoute(builder: (_) => BookingUI(arguments: settings.arguments));
          case ROUTE_TRANSAKSI:
            return MaterialPageRoute(builder: (_) => TransaksiUI());
          default:
            return MaterialPageRoute(builder: (_) => SplashScreenUI());
        }
      },
    );
  }
}
