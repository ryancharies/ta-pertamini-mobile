import 'dart:convert';

import 'package:pertamini/network/response/LoginResponse.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppPreference {
  static SharedPreferences _preferences;

  static const ACCOUNT_PREFERENCE = "ACCOUNT_PREFERENCE";

  static Future<void> _init() async {
    if(_preferences == null){
      _preferences = await SharedPreferences.getInstance();
    }
  }

  static Future<UserData> getUser() async {
    await _init();

    if(_preferences.containsKey(ACCOUNT_PREFERENCE)){
      return UserData.fromJson(jsonDecode(_preferences.getString(ACCOUNT_PREFERENCE)));
    }else {
      return null;
    }
  }

  static Future<bool> saveUser(UserData user) async {
    await _init();

    return _preferences.setString(ACCOUNT_PREFERENCE, user.toJson());
  }

  static Future<bool> removeUser() async {
    await _init();

    return _preferences.remove(ACCOUNT_PREFERENCE);
  }
}