import 'package:pertamini/ancestor/BaseState.dart';
import 'package:pertamini/extension/Size.dart' as s;

///
/// Extension of BaseState class
///
extension BaseStateExt on BaseState {
  width(double size){
    return s.adaptiveWidth(this.context, size);
  }

  height(double size){
    return s.adaptiveHeight(this.context, size);
  }

  sp(double size){
    return s.sp(this.context, size);
  }
}