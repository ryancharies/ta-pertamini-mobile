import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

///
/// Adaptive Size depends on device size
/// read more about it in https://pub.dev/packages/flutter_screenutil
///
double adaptiveWidth(BuildContext context, double size, {bool withRatio = false}){
    ScreenUtil.init(
        context,
        width: 414,
        height: 736,
        allowFontScaling: false);

  return ScreenUtil().setWidth(size);
}

double adaptiveHeight(BuildContext context, double size, {bool withRatio = false}){
  ScreenUtil.init(
      context,
      width: 414,
      height: 736,
      allowFontScaling: false);

  return ScreenUtil().setHeight(size);
}

double sp(BuildContext context, double size){
  var scaleFactor = MediaQuery.of(context).textScaleFactor;

  return scaleFactor <= 1 ? adaptiveWidth(context, size) : adaptiveWidth(context, size) * scaleFactor;
}