import 'package:pertamini/network/request/ApiRepository.dart';

import 'BaseState.dart';

class BasePresenter {
  final BaseState state;
  ApiRepository api;

  BasePresenter(this.state){
    api = ApiRepository(state);
  }
}