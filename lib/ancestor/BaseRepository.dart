import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:pertamini/ancestor/BaseResponse.dart';

import 'BaseState.dart';

///
/// Kelas induk yang menangani proses komunikasi dengan backend
///
class BaseRepository{
  static const GET = 0;
  static const POST = 1;
  static const PUT = 2;
  static const DELETE = 3;

  BaseState _baseState;

  Dio dio = Dio();

  BaseRepository(this._baseState, {String baseUrl = ""}){
    dio.interceptors.add(LogInterceptor(responseBody: true));
    dio.options.baseUrl = baseUrl;
    dio.options.connectTimeout = 60000;
    dio.options.receiveTimeout = 60000;
    dio.options.sendTimeout = 60000;
  }

  Future<bool> connectivityChecker() async {
    var connectionStatus = await (Connectivity().checkConnectivity());
    return connectionStatus != ConnectivityResult.none;
  }

  ///
  /// Execute GET Request Method
  ///
  /// @params url = path
  /// @params params = parameter, ex ?username=xxx&password=xxx
  /// @params typeRequest = TAG of request, it will passed when error happens, obtain it by overriding
  /// @params cancelToken = Token of cancellable request, read more about it in dio documentation
  /// @params contentType = Header contentType
  ///
  /// shouldShowLoading(), shouldHideLoading()
  /// onRequestTimeOut(), onNoConnection(), onResponseError(), onUnknownError()
  /// in BaseState class
  ///
  Future<Response<String>> get(String url,
      Map<String, String> params,
      int typeRequest,
      {
        CancelToken cancelToken,
        String contentType = "application/json"
      }) {
    return _execute(GET, url, params, typeRequest, cancelToken: cancelToken, contentType: contentType);
  }

  ///
  /// Execute POST Request Method
  ///
  /// @params url = path
  /// @params params = parameter, ex ?username=xxx&password=xxx
  /// @params typeRequest = TAG of request, it will passed when error happens, obtain it by overriding
  /// @params cancelToken = Token of cancellable request, read more about it in dio documentation
  /// @params contentType = Header contentType
  ///
  /// shouldShowLoading(), shouldHideLoading()
  /// onRequestTimeOut(), onNoConnection(), onResponseError(), onUnknownError()
  /// in BaseState class
  ///
  Future<Response<String>> post(String url,
      Map<String, dynamic> params,
      int typeRequest,
      {
        CancelToken cancelToken,
        String contentType = "application/json"
      }){
    return _execute(POST, url, params, typeRequest, cancelToken: cancelToken, contentType: contentType);
  }

  Future<Response<String>> _execute(int method, String url, Map<String, dynamic> params,
      int typeRequest, { CancelToken cancelToken, String contentType = "application/json"}) async {
    _baseState.shouldShowLoading(typeRequest);

    var isConnected = await connectivityChecker();
    if(!isConnected){
      _baseState.shouldHideLoading(typeRequest);
      _baseState.onNoConnection(typeRequest);
      return null;
    }

    try{
      Response<String> response;

      if(method == GET){
        response = await dio.get<String>(url, queryParameters: params, options: Options(
          contentType: contentType,
        ), cancelToken: cancelToken);
      }else if(method == POST){
        response = await dio.post<String>(url, data: params, options: Options(
          contentType: contentType,
        ), cancelToken: cancelToken);
      }else if(method == PUT){
        response = await dio.put<String>(url, data: params, options: Options(
            contentType: contentType
        ), cancelToken: cancelToken);
      }else if(method == DELETE){
        response = await dio.delete<String>(url, data: params, options: Options(
            contentType: contentType
        ), cancelToken: cancelToken);
      }else {
        return null;
      }

      var baseResponse = BaseResponse.fromJson(jsonDecode(response.data));
      if(baseResponse.error){
        throw ResponseException(baseResponse.message);
      }

      _baseState.shouldHideLoading(typeRequest);
      return response;
    } on DioError catch(e){
      if(e.type == DioErrorType.CANCEL) {
        _baseState.shouldHideLoading(typeRequest);
      }else if(e.type == DioErrorType.CONNECT_TIMEOUT ||
          e.type == DioErrorType.RECEIVE_TIMEOUT ||
          e.type == DioErrorType.SEND_TIMEOUT){
        _baseState.shouldHideLoading(typeRequest);
        _baseState.onRequestTimeOut(typeRequest);
      }else {
        if(e.message.contains("SocketException")){
          _baseState.shouldHideLoading(typeRequest);
          _baseState.onNoConnection(typeRequest);
        }else {
          _baseState.shouldHideLoading(typeRequest);
          _baseState.onUnknownError(typeRequest, e.message);
        }
      }
    } on ResponseException catch(e){
      _baseState.shouldHideLoading(typeRequest);
      _baseState.onResponseError(typeRequest, e);
    }

    return null;
  }
}