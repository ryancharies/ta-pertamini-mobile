import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pertamini/ancestor/BaseResponse.dart';
import 'package:pertamini/custom/dialog/LoadingDialog.dart';
import 'package:pertamini/extension/ErrorMessaging.dart';

///
/// Induk dari class View
/// Digunakan untuk mempermudah pembuatan aplikasi karena
/// memiliki fungsi fungsi yang sering digunakan
///
abstract class BaseState<T extends StatefulWidget> extends State<T> {
  static const DEFAULT_ALERT_DIALOG_TAG = 0x111;
  static const DEFAULT_DIALOG_TAG = 0x112;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_){
      afterWidgetBuilt();
    });
  }

  ///
  /// Dipanggil ketika view telah di-render
  ///
  void afterWidgetBuilt(){

  }

  ///
  /// Menampilkan Loading dialog
  ///
  void shouldShowLoading(int typeRequest){
    showDialog(context: context,
      barrierDismissible: false,
      builder: (BuildContext buildContext) => LoadingDialog(),
    );
  }

  ///
  /// Close Loading Dialog
  ///
  void shouldHideLoading(int typeRequest){
    Navigator.of(context).pop();
  }

  ///
  /// Menampilkan error dialog
  /// contohnya: Wrong Username / Password when login, etc
  ///
  ///
  void onResponseError(int typeRequest, ResponseException exception){
    dialog(msg: exception.msg);
  }

  ///
  /// Menampilkan error dialog Request Time Out
  ///
  void onRequestTimeOut(int typeRequest){
    dialog(msg: ErrorMessaging.REQUEST_TIME_OUT_MESSAGE);
  }

  ///
  /// Menampilkan error dialog tidak ada koneksi internet
  /// ketika tidak terhubung ke jaringan internet
  ///
  void onNoConnection(int typeRequest){
    dialog(msg: ErrorMessaging.NO_CONNECTION_MESSAGE);
  }

  ///
  /// Menampilkan error http dialog
  ///
  void onUnknownError(int typeRequest, String msg){
    dialog(msg: msg);
  }

  ///
  /// Menampilkan dialog
  ///
  void dialog({dynamic tag = DEFAULT_DIALOG_TAG, String title = ErrorMessaging.ERROR_TITLE_MESSAGE,
    String msg = ErrorMessaging.DEFAULT_MESSAGE,
    String actionMsg = ErrorMessaging.CLOSE_ACTION_MESSAGE, Function() action}){
    if(action == null){
      action = () => Navigator.of(context).pop();
    }

    dynamic result = showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext c) => AlertDialog(
        title: Text(title, style: TextStyle(fontSize: 20, color: Colors.black)),
        content: Text(msg, style: TextStyle(fontSize: 16, color: Color(0x8a000000))),
        actions: <Widget>[
          FlatButton(
            child: Text(actionMsg),
            onPressed: action,
          ),
        ],
      ),
    );

    if(result != null){
      onDialogResult(tag, result);
    }
    onDialogClosed(tag);
  }

  ///
  /// Dipanggil ketika view dibuka kembali setelah pergi ke view lain
  ///
  void onNavigationResume(String from){

  }


  ///
  /// Dipanggil ketika view mengirim data ke view sebelumnya
  ///
  void onNavigationResult(String from, dynamic result){

  }

  ///
  /// Fungsi untuk berpindah view
  ///
  void navigateTo(String destination, {bool singleTop = false, dynamic arguments}) async {
    if(singleTop){
      Navigator.of(context).pushNamedAndRemoveUntil(destination, (_) => false, arguments: arguments);
    }else {
      var result = await Navigator.of(context)
          .pushNamed(destination, arguments: arguments);

      if(result != null){
        onNavigationResult(destination, result);
      }
      onNavigationResume(destination);
    }
  }

  ///
  /// Fungsi untuk berpindah view dan menutup view saat ini
  ///
  void navigateAndFinishCurrent(String destination, {dynamic arguments}) async {
    Navigator.of(context).popAndPushNamed(destination, arguments: arguments);
  }

  ///
  /// Fungs untuk menutup view
  ///
  void finish({dynamic result}){
    Navigator.of(context).pop(result);
  }

  ///
  /// Fungsi untuk menampilkan popup dialog
  ///
  void openDialog({@required dynamic tag,
    @required BuildContext context,
    @required Widget Function(BuildContext context) builder}) async {

    dynamic result;

    if(Platform.isIOS){
      result = await showCupertinoDialog(
          context: context,
          builder: builder
      );
    }else {
      result = await showDialog(
          context: context,
          builder: builder
      );
    }

    if(result != null){
      onDialogResult(tag, result);
    }
    onDialogClosed(tag);
  }

  ///
  /// Dipanggil ketika dialog yang ditampilkan memberikan return value
  ///
  void onDialogResult(dynamic tag, dynamic result){

  }

  ///
  /// Dipanggil ketika menutup dialog yang telah dibuka
  ///
  void onDialogClosed(dynamic tag){

  }
}

