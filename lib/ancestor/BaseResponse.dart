
///
/// You should Extends this class when creating another
/// class that only contains information from APIs/Webservices
///
class BaseResponse {
  bool error = false;
  String message = "";

  BaseResponse();

  BaseResponse.fromJson(Map<String, dynamic> json){
    error = json["error"] ?? false;
    message = json["message"] ?? "";
  }
}

class ResponseException implements Exception{
  final String msg;

  ResponseException(this.msg);
}