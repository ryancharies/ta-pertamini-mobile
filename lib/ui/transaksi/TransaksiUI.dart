import 'package:flutter/material.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:pertamini/ancestor/BaseState.dart';
import 'package:pertamini/custom/dialog/DetailTransaksiDialog.dart';
import 'package:pertamini/extension/BaseStateExt.dart';
import 'package:pertamini/custom/view/text/StyledText.dart';
import 'package:pertamini/network/response/DataPenjualanResponse.dart';
import 'package:pertamini/ui/home/adapter/HomeTransaksiAdapter.dart';
import 'package:pertamini/ui/transaksi/TransaksiPresenter.dart';
import 'package:pertamini/utils/ColorUtils.dart';
import 'package:pertamini/utils/FontUtils.dart';


class TransaksiUI extends StatefulWidget {
  @override
  _TransaksiUIState createState() => _TransaksiUIState();
}

class _TransaksiUIState extends BaseState<TransaksiUI> {
  TransaksiPresenter _presenter;
  RequestWrapper<DataPenjualanResponse> _penjualanWrapper = RequestWrapper();

  @override
  void initState() {
    super.initState();
    _presenter = TransaksiPresenter(this);
  }

  @override
  void afterWidgetBuilt() => _presenter.executeGetPenjualan(_penjualanWrapper);

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorUtils.bgEpayGradientStart,
        leading: IconButton(
          onPressed: () => finish(),
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
        ),
        title: StyledText("Transaksi",
          color: Colors.white,
          fontWeight: FontType.bold,
          fontFamily: FontFamily.gotham,
        ),
      ),
      body: RequestWrapperWidget<DataPenjualanResponse>(
        requestWrapper: _penjualanWrapper,
        placeholder: (_) => ListView.builder(
          itemCount: 2,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (_, __) => ShimmerHomeTransaksi(),
        ),
        builder: (_, data) => ListView.builder(
          padding: EdgeInsets.all(width(10)),
          itemCount: data.data.length,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (_, index) => HomeTransaksiItem(data: data.data[index], onSelected: (it){
            showModalBottomSheet(context: context,
              backgroundColor: Colors.transparent,
              builder: (_) => DetailTransaksiDialog(data: it),
            );
          }),
        ),
      ),
    );
  }
}
