import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:pertamini/ancestor/BasePresenter.dart';
import 'package:pertamini/ancestor/BaseState.dart';
import 'package:pertamini/network/response/DataPenjualanResponse.dart';
import 'package:pertamini/preferences/AppPreferences.dart';

class TransaksiPresenter extends BasePresenter {
  static const REQUEST_GET_PENJUALAN = 0;

  TransaksiPresenter(BaseState state) : super(state);

  void executeGetPenjualan(RequestWrapper<DataPenjualanResponse> wrapper) async {
    var user = await AppPreference.getUser();

    wrapper.doRequest();
    api.executeGetPenjualan(REQUEST_GET_PENJUALAN, user.rfid, wrapper.finishRequest);
  }
}