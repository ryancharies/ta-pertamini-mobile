import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:pertamini/custom/dialog/CustomAlertDialog.dart';
import 'package:pertamini/network/response/LoginResponse.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pertamini/ancestor/BaseResponse.dart';
import 'package:pertamini/ancestor/BaseState.dart';
import 'package:pertamini/custom/view/text/StyledText.dart';
import 'package:pertamini/extension/BaseStateExt.dart';
import 'package:pertamini/network/response/DataLayananResponse.dart';
import 'package:pertamini/ui/booking/BookingDelegate.dart';
import 'package:pertamini/ui/booking/BookingPresenter.dart';
import 'package:pertamini/ui/booking/adapter/BookingAdapter.dart';
import 'package:pertamini/utils/ColorUtils.dart';
import 'package:pertamini/utils/FontUtils.dart';

class BookingUI extends StatefulWidget {
  static const PAYMENT_CASH = 2;
  static const PAYMENT_EPAY = 3;

  final DataLayananResponse arguments;

  BookingUI({@required this.arguments});

  @override
  _BookingUIState createState() => _BookingUIState();
}

class _BookingUIState extends BaseState<BookingUI> implements BookingDelegate{
  static const TAG_CONFIRM = 0;
  static const TAG_SUCCESS = 1;

  BookingPresenter _presenter;
  RequestWrapper<UserData> _userWrapper = RequestWrapper();

  int _purchaseType = BookingItem.PURCHASE_CASH;
  int _paymentType = BookingUI.PAYMENT_EPAY;

  Set<DataLayanan> _selectedLayanan = Set();

  @override
  void initState() {
    super.initState();
    _presenter = BookingPresenter(this, this);
  }

  @override
  void onBookingSuccess(BaseResponse response) {
    openDialog(tag: TAG_SUCCESS,
      context: context,
      builder: (_) => CustomAlertDialog(title: "Pesan BBM",
        msg: "Pesanan anda telah berhasil dibuat segera kunjungi SPBU di tempat anda mendaftar",
        negativeTitle: "",
      ),
    );
  }

  @override
  void onDialogResult(tag, result) {
    if(tag == TAG_CONFIRM){
      if(result){
        _presenter.executeBooking(_selectedLayanan, _paymentType);
      }
    }
  }

  @override
  void onDialogClosed(tag) {
    if(tag == TAG_SUCCESS){
      finish(result: true);
    }
  }

    @override
  void afterWidgetBuilt(){
    _presenter.executeGetUser(_userWrapper);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorUtils.bgEpayGradientStart,
        leading: IconButton(
          onPressed: () => finish(),
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
        ),
        title: StyledText("Pesan BBM",
          color: Colors.white,
          fontFamily: FontFamily.gotham,
          fontWeight: FontType.bold,
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(width(20)),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  RequestWrapperWidget<UserData>(
                    requestWrapper: _userWrapper,
                    placeholder: (_,) => StyledText("Saldo Epay",
                      size: sp(16),
                      fontWeight: FontType.bold,
                      fontFamily: FontFamily.gotham,
                      color: ColorUtils.grey9f9f,
                    ),
                    builder: (_, user) => StyledText("Saldo Epay Rp. ${double.parse(user.epay).toInt()}",
                      size: sp(16),
                      fontWeight: FontType.bold,
                      fontFamily: FontFamily.gotham,
                      color: ColorUtils.blue2399,
                    ),
                  ),                  
                ],
              ),
              SizedBox(height: width(10)),
              Container(
                width: double.infinity,
                height: 1,
                color: ColorUtils.greybebe,
              ),
              SizedBox(height: width(20)),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      StyledText("Metode Pembelian",
                        size: sp(12),
                        fontFamily: FontFamily.roboto,
                        fontWeight: FontType.bold,
                        color: ColorUtils.grey1616.withOpacity(0.41),
                      ),
                      SizedBox(height: width(10)),
                      Row(
                        children: <Widget>[
                          InkWell(
                            onTap: () => setState(() => _purchaseType = BookingItem.PURCHASE_CASH),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  width: sp(16),
                                  height: sp(16),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(sp(16)),
                                    border: Border.all(color: ColorUtils.bgEpayGradientStart),
                                  ),
                                  child: Container(
                                    width: sp(8),
                                    height: sp(8),
                                    decoration: BoxDecoration(
                                      color: _purchaseType == BookingItem.PURCHASE_CASH ? ColorUtils.bgEpayGradientStart : Colors.white,
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                ),
                                SizedBox(width: width(5)),
                                StyledText("Tunai",
                                  size: sp(14),
                                  color: ColorUtils.grey9f9f,
                                  fontFamily: FontFamily.gotham,
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                          ),

                          SizedBox(width: width(10)),

                          InkWell(
                            onTap: () => setState(() => _purchaseType = BookingItem.PURCHASE_LITRE),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  width: sp(16),
                                  height: sp(16),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(sp(16)),
                                    border: Border.all(color: ColorUtils.bgEpayGradientStart),
                                  ),
                                  child: Container(
                                    width: sp(8),
                                    height: sp(8),
                                    decoration: BoxDecoration(
                                      color: _purchaseType == BookingItem.PURCHASE_LITRE ? ColorUtils.bgEpayGradientStart : Colors.white,
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                ),
                                SizedBox(width: width(5)),
                                StyledText("Liter",
                                  size: sp(14),
                                  color: ColorUtils.grey9f9f,
                                  fontFamily: FontFamily.gotham,
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      StyledText("Metode Pembayaran",
                        size: sp(12),
                        fontFamily: FontFamily.roboto,
                        fontWeight: FontType.bold,
                        color: ColorUtils.grey1616.withOpacity(0.41),
                      ),
                      SizedBox(height: width(10)),
                      Row(
                        children: <Widget>[
                          InkWell(
                            onTap: () => setState(() => _paymentType = BookingUI.PAYMENT_CASH),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  width: sp(16),
                                  height: sp(16),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(sp(16)),
                                    border: Border.all(color: ColorUtils.bgEpayGradientStart),
                                  ),
                                  child: Container(
                                    width: sp(8),
                                    height: sp(8),
                                    decoration: BoxDecoration(
                                      color: _paymentType == BookingUI.PAYMENT_CASH ? ColorUtils.bgEpayGradientStart : Colors.white,
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                ),
                                SizedBox(width: width(5)),
                                StyledText("Tunai",
                                  size: sp(14),
                                  color: ColorUtils.grey9f9f,
                                  fontFamily: FontFamily.gotham,
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                          ),

                          SizedBox(width: width(10)),

                          InkWell(
                            onTap: () => setState(() => _paymentType = BookingUI.PAYMENT_EPAY),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  width: sp(16),
                                  height: sp(16),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(sp(16)),
                                    border: Border.all(color: ColorUtils.bgEpayGradientStart),
                                  ),
                                  child: Container(
                                    width: sp(8),
                                    height: sp(8),
                                    decoration: BoxDecoration(
                                      color: _paymentType == BookingUI.PAYMENT_EPAY ? ColorUtils.bgEpayGradientStart : Colors.white,
                                      shape: BoxShape.circle,
                                    ),
                                  ),
                                ),
                                SizedBox(width: width(5)),
                                StyledText("Epay",
                                  size: sp(14),
                                  color: ColorUtils.grey9f9f,
                                  fontFamily: FontFamily.gotham,
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  )
                ],
              ),
              SizedBox(height: width(30)),
              ListView.builder(
                itemCount: widget.arguments.data.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (_, index) => BookingItem(
                  data: widget.arguments.data[index],
                  purchaseType: _purchaseType,
                  onChanged: (it){
                    if(it.liter == 0 || it.bayar == 0){
                      setState(() => _selectedLayanan.remove(it));
                    }else {
                      setState(() => _selectedLayanan.add(it));
                    }
                  },
                ),
              ),
              Container(
                width: double.infinity,
                height: 1,
                color: ColorUtils.greybebe,
              ),
              SizedBox(height: width(30)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  StyledText("Total Bayar",
                    size: sp(18),
                    fontFamily: FontFamily.gotham,
                    fontWeight: FontType.superbold,
                  ),
                  StyledText("Rp. ${_getTotalBayar()}",
                    size: sp(18),
                    fontFamily: FontFamily.gotham,
                    fontWeight: FontType.superbold,
                  )
                ],
              ),
              SizedBox(height: width(20)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  StyledText("Total Liter",
                    size: sp(18),
                    fontFamily: FontFamily.gotham,
                    fontWeight: FontType.superbold,
                  ),
                  StyledText("${_getTotalLitre()} L",
                    size: sp(18),
                    fontFamily: FontFamily.gotham,
                    fontWeight: FontType.superbold,
                  )
                ],
              ),
              SizedBox(height: width(40)),
              MaterialButton(
                onPressed: () {
                  openDialog(tag: TAG_CONFIRM, context: context,
                    builder: (_) => CustomAlertDialog(
                      title: "Pesan BBM",
                      msg: "Apakah yakin dengan pesanan anda ?",
                    ),
                  );
                },
                color: ColorUtils.bgEpayGradientStart,
                minWidth: double.infinity,
                height: sp(45),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(sp(10)),
                ),
                child: StyledText("PESAN",
                  fontFamily: FontFamily.roboto,
                  size: sp(16),
                  color: Colors.white,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  String _getTotalBayar(){
    double totalBayar = 0;

    _selectedLayanan.forEach((it){
      totalBayar += it.liter * it.hargaLiter;
    });

    return totalBayar.toStringAsFixed(0);
  }

  String _getTotalLitre(){
    double totalLitre = 0;

    _selectedLayanan.forEach((it){
      totalLitre += it.liter;
    });

    return totalLitre.toStringAsFixed(2);
  }
}
