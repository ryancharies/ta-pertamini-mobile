import 'package:flutter/material.dart';
import 'package:pertamini/ancestor/BaseState.dart';
import 'package:pertamini/custom/view/text/StyledText.dart';
import 'package:pertamini/extension/BaseStateExt.dart';
import 'package:pertamini/network/response/DataLayananResponse.dart';
import 'package:pertamini/utils/ColorUtils.dart';
import 'package:pertamini/utils/FontUtils.dart';

class BookingItem extends StatefulWidget {
  static const PURCHASE_CASH = 0;
  static const PURCHASE_LITRE = 1;

  final DataLayanan data;
  final int purchaseType;
  final Function(DataLayanan) onChanged;

  BookingItem({@required this.data, @required this.purchaseType, @required this.onChanged});

  @override
  _BookingItemState createState() => _BookingItemState();
}

class _BookingItemState extends BaseState<BookingItem> {

  TextEditingController _cashController = TextEditingController();
  TextEditingController _litreController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _cashController.addListener((){
      if(widget.purchaseType == BookingItem.PURCHASE_CASH) {
        var text = _cashController.text;
        if(text != "") {
          var cash = int.parse(_cashController.text);
          var litre = cash / widget.data.hargaLiter;

          _litreController.text = litre.toStringAsFixed(2);

          widget.data.liter = litre;
          widget.data.bayar = cash;
        }else {
          _litreController.text = "";
          widget.data.liter = 0;
          widget.data.bayar = 0;
        }

        widget.onChanged(widget.data);
      }
    });

    _litreController.addListener((){
      if(widget.purchaseType == BookingItem.PURCHASE_LITRE) {
        var text = _litreController.text;
        if(text != ""){
          var litre = double.parse(_litreController.text);
          var cash = litre * widget.data.hargaLiter;

          _cashController.text = cash.toInt().toString();

          widget.data.liter = litre;
          widget.data.bayar = cash.toInt();
        }else {
          _cashController.text = "";
          widget.data.liter = 0;
          widget.data.bayar = 0;
        }

        widget.onChanged(widget.data);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        StyledText(widget.data.keterangan,
          size: sp(12),
          fontFamily: FontFamily.roboto,
          fontWeight: FontType.bold,
          color: ColorUtils.grey1616.withOpacity(0.41),
        ),
        SizedBox(height: width(15)),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: width(50),
              alignment: Alignment.center,
              child: StyledText("Rp",
                size: width(16),
                color: ColorUtils.greyde000000,
                fontFamily: FontFamily.roboto,
              ),
            ),
            SizedBox(width: width(10)),
            Container(
              width: width(140),
              height: width(50),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(sp(5)),
                color: widget.purchaseType == BookingItem.PURCHASE_CASH ? ColorUtils.greyf2f2 : ColorUtils.greybebe,
              ),
              child: TextFormField(
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.number,
                controller: _cashController,
                readOnly: widget.purchaseType == BookingItem.PURCHASE_CASH ? false : true,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  border: InputBorder.none,
                ),
              ),
            ),
            Spacer(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: width(140),
                  height: width(50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(sp(5)),
                    color: widget.purchaseType == BookingItem.PURCHASE_LITRE ? ColorUtils.greyf2f2 : ColorUtils.greybebe,
                  ),
                  child: TextFormField(
                    textInputAction: TextInputAction.done,
                    keyboardType: TextInputType.number,
                    controller: _litreController,
                    readOnly: widget.purchaseType == BookingItem.PURCHASE_LITRE ? false : true,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(10),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                SizedBox(height: width(5)),
                StyledText("*max 80 L",
                  color: ColorUtils.redLitre,
                  fontFamily: FontFamily.gotham,
                  fontWeight: FontType.thin,
                  size: sp(12),
                )
              ],
            ),
            SizedBox(width: width(10)),
            Container(
              height: width(50),
              alignment: Alignment.center,
              child: StyledText("L",
                size: width(16),
                color: ColorUtils.greyde000000,
                fontFamily: FontFamily.roboto,
              ),
            ),
          ],
        ),
        SizedBox(height: width(20)),
      ],
    );
  }
}

