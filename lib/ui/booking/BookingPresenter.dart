import 'package:fluttertoast/fluttertoast.dart';
import 'package:pertamini/ancestor/BasePresenter.dart';
import 'package:pertamini/ancestor/BaseState.dart';
import 'package:pertamini/network/response/DataLayananResponse.dart';
import 'package:pertamini/preferences/AppPreferences.dart';
import 'package:pertamini/ui/booking/BookingDelegate.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:pertamini/network/response/LoginResponse.dart';
import 'package:pertamini/ui/booking/BookingUI.dart';

class BookingPresenter extends BasePresenter {
  static const REQUEST_BOOKING = 0;

  BookingDelegate _delegate;

  BookingPresenter(BaseState state, this._delegate) : super(state);

  void executeBooking(Set<DataLayanan> data, int paymentType) async {
    if(data.length == 0){
      Fluttertoast.showToast(msg: "Belum ada layanan yang ingin dibeli");
      return;
    }    

    var user = await AppPreference.getUser();
    var params = Map<String, dynamic>();
    var detail = List<Map<String, dynamic>>();

    params["pengecer_id"] = user.id;
    params["pengecer_nama"] = user.nama;
    params["pengecer_rfid"] = user.rfid;
    params["metode_pembayaran"] = paymentType == BookingUI.PAYMENT_CASH ? "tunai" : "epay";

    params["total_liter"] = 0.0;
    params["total_bayar"] = 0;

    data.forEach((it){
      params["total_liter"] += it.liter;
      params["total_bayar"] += it.bayar;

      var jsonDetail = Map<String, dynamic>();
      jsonDetail["layanan_id"] = it.id;
      jsonDetail["keterangan"] = it.keterangan;
      jsonDetail["slug"] = it.slug;
      jsonDetail["liter"] = it.liter;
      jsonDetail["harga_liter"] = it.hargaLiter;
      jsonDetail["bayar"] = it.bayar;

      detail.add(jsonDetail);
    });

    params["detail"] = detail;
    params['epay'] = paymentType == BookingUI.PAYMENT_EPAY ? params['total_bayar'] : 0;
    params['cash'] = paymentType == BookingUI.PAYMENT_CASH ? params['total_bayar'] : 0;
    params['status'] = 'proses';

    double epayTemp = double.parse(user.epay) - params["total_bayar"];
    if(epayTemp < 0 && paymentType == BookingUI.PAYMENT_EPAY){
      Fluttertoast.showToast(msg: "Maaf, saldo epay anda tidak mencukupi");
      return;
    }

    api.executeBooking(REQUEST_BOOKING, params, _delegate.onBookingSuccess);
  }

  void executeGetUser(RequestWrapper<UserData> wrapper) async {
    wrapper.doRequest();
    wrapper.finishRequest(await AppPreference.getUser());
  }
}