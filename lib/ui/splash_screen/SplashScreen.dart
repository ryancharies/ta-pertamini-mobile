import 'package:mcnmr_common_ext/FutureDelayed.dart';
import 'package:pertamini/ancestor/BaseState.dart';
import 'package:flutter/material.dart';
import 'package:pertamini/main.dart';
import 'package:pertamini/preferences/AppPreferences.dart';
import 'package:pertamini/utils/ImageUtils.dart';

class SplashScreenUI extends StatefulWidget {
  @override
  _SplashScreenUIState createState() => _SplashScreenUIState();
}

class _SplashScreenUIState extends BaseState<SplashScreenUI> {

  @override
  void afterWidgetBuilt() => delay(2000, () async {
    if(await AppPreference.getUser() != null){
      navigateTo(MyApp.ROUTE_HOME, singleTop: true);
    }else {
      navigateTo(MyApp.ROUTE_LOGIN, singleTop: true);
    }
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset(ImageUtils.fuel),
      ),
    );
  }
}
