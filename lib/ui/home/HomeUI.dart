import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:pertamini/ancestor/BaseResponse.dart';
import 'package:pertamini/ancestor/BaseState.dart';
import 'package:pertamini/custom/dialog/CustomAlertDialog.dart';
import 'package:pertamini/custom/dialog/TopupDialog.dart';
import 'package:pertamini/custom/view/text/StyledText.dart';
import 'package:pertamini/extension/BaseStateExt.dart';
import 'package:pertamini/main.dart';
import 'package:pertamini/network/response/DataLayananResponse.dart';
import 'package:pertamini/network/response/DataPenjualanResponse.dart';
import 'package:pertamini/network/response/DataVolumeResponse.dart';
import 'package:pertamini/network/response/LoginResponse.dart';
import 'package:pertamini/preferences/AppPreferences.dart';
import 'package:pertamini/ui/home/HomeDelegate.dart';
import 'package:pertamini/ui/home/HomePresenter.dart';
import 'package:pertamini/ui/home/adapter/HomeTransaksiAdapter.dart';
import 'package:pertamini/ui/home/adapter/VolumeTangkiAdapter.dart';
import 'package:pertamini/utils/ColorUtils.dart';
import 'package:pertamini/utils/FontUtils.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeUI extends StatefulWidget {
  @override
  _HomeUIState createState() => _HomeUIState();
}

class _HomeUIState extends BaseState<HomeUI> implements HomeDelegate{
  static const TAG_DIALOG_LOGOUT = 0;
  static const TAG_DIALOG_TOPUP = 1;
  static const TAG_DIALOG_SUCCESS_TOPUP = 2;

  HomePresenter _presenter;
  RequestWrapper<DataVolumeResponse> _volumeWrapper = RequestWrapper();
  RequestWrapper<DataPenjualanResponse> _penjualanWrapper = RequestWrapper();
  RequestWrapper<UserData> _userWrapper = RequestWrapper();

  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    _presenter = HomePresenter(this, this);
  }

  @override
  void afterWidgetBuilt(){
    _presenter.executeGetUser(_userWrapper);
    _presenter.executeGetVolume(_volumeWrapper, _userWrapper);
    _presenter.executeGetPenjualan(_penjualanWrapper);
  }

  @override
  void onDialogResult(tag, result) async {
    if(tag == TAG_DIALOG_TOPUP){
      _presenter.executeTopup(result);
    }else if(tag == TAG_DIALOG_LOGOUT){
      await AppPreference.removeUser();
      navigateTo(MyApp.ROUTE_LOGIN, singleTop: true);
    }
  }

  @override
  void onSuccessTopup(BaseResponse response) {
    openDialog(tag: TAG_DIALOG_SUCCESS_TOPUP, 
      context: context,
      builder: (_) => CustomAlertDialog(title: "Top Up",
        msg: response.message,
        negativeTitle: "",
      ),
    );
  }

  @override
  void onNavigationResult(String from, result) {
    if(from == MyApp.ROUTE_BOOKING){
      if(result){
        afterWidgetBuilt();
      }
    }
  }  

  @override
  void shouldHideLoading(int typeRequest) {
    if(typeRequest == HomePresenter.REQUEST_GET_LAYANAN){
      super.shouldHideLoading(typeRequest);
    }
  }

  @override
  void shouldShowLoading(int typeRequest) {
    if(typeRequest == HomePresenter.REQUEST_GET_LAYANAN){
      super.shouldShowLoading(typeRequest);
    }
  }

  @override
  void onSuccessGetLayanan(DataLayananResponse response) =>
      navigateTo(MyApp.ROUTE_BOOKING, arguments: response);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.bgColor,
      body: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).padding.top,
            width: double.infinity,
            color: Colors.white,
          ),
          Expanded(
            child: SmartRefresher(
              controller: _refreshController,
              header: MaterialClassicHeader(
                backgroundColor: ColorUtils.bgEpayGradientStart,
                color: Colors.white,
              ),
              onRefresh: (){                
                _presenter.executeGetVolume(_volumeWrapper, _userWrapper);
                _presenter.executeGetPenjualan(_penjualanWrapper);
                _refreshController.refreshCompleted();
              },
              child: SingleChildScrollView(
                padding: EdgeInsets.all(width(20)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        StyledText("Agen Pertamini",
                          size: sp(24),
                          color: ColorUtils.grey4350,
                          fontWeight: FontType.bold,
                          fontFamily: FontFamily.gotham,
                        ),
                        Spacer(),
                        InkWell(
                          onTap: (){
                            openDialog(tag: TAG_DIALOG_LOGOUT,
                              context: context,
                              builder: (_) => CustomAlertDialog(
                                title: "Keluar",
                                msg: "Apakah anda yakin keluar dari aplikasi ?",
                              ),
                            );
                          },
                          child: Padding(
                            padding: EdgeInsets.all(width(10)),
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.exit_to_app),
                                SizedBox(width: width(10)),
                                StyledText("Keluar",
                                  size: sp(16),
                                  fontWeight: FontType.superbold,
                                  fontFamily: FontFamily.gotham,
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: width(20)),
                    Container(
                      width: double.infinity,
                      padding: EdgeInsets.all(width(20)),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [ColorUtils.bgEpayGradientStart, ColorUtils.bgEpayGradientEnd],
                            stops: [0, 1]
                        ),
                        borderRadius: BorderRadius.circular(width(15)),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(Icons.account_balance_wallet, color: Colors.white),
                              SizedBox(width: width(20)),
                              StyledText("Epay",
                                size: sp(14),
                                color: Colors.white,
                                fontFamily: FontFamily.gotham,
                                fontWeight: FontType.light,
                              )
                            ],
                          ),
                          SizedBox(height: width(15)),
                          RequestWrapperWidget<UserData>(
                            requestWrapper: _userWrapper,
                            placeholder: (_,) => StyledText("Rp. ",
                              size: sp(32),
                              color: Colors.white,
                              fontFamily: FontFamily.gotham,
                              fontWeight: FontType.light,
                            ),
                            builder: (_, user) => StyledText("Rp. ${double.parse(user.epay).toInt()}",
                              size: sp(32),
                              color: Colors.white,
                              fontFamily: FontFamily.gotham,
                              fontWeight: FontType.light,
                            ),
                          ),
                          SizedBox(height: width(15)),
                          Container(
                            width: double.infinity,
                            height: 1,
                            color: Colors.white,
                          ),
                          SizedBox(height: width(15)),
                          IntrinsicHeight(
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: InkWell(
                                    onTap: () => openDialog(
                                      tag: TAG_DIALOG_TOPUP,
                                      context: context,
                                      builder: (_) => TopupDialog(),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(vertical: width(5)),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.add_circle_outline, color: Colors.white),
                                          SizedBox(width: width(15)),
                                          StyledText("Top Up",
                                            size: sp(16),
                                            color: Colors.white,
                                            fontFamily: FontFamily.gotham,
                                            fontWeight: FontType.light,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: width(10)),
                                Container(
                                  width: 1,
                                  height: double.infinity,
                                  color: Colors.white,
                                ),
                                SizedBox(width: width(10)),
                                Expanded(
                                  child: InkWell(
                                    onTap: () => _presenter.executeGetLayanan(),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(vertical: width(5)),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.credit_card, color: Colors.white),
                                          SizedBox(width: width(15)),
                                          StyledText("Pesan BBM",
                                            size: sp(16),
                                            color: Colors.white,
                                            fontFamily: FontFamily.gotham,
                                            fontWeight: FontType.light,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: width(30)),
                    StyledText("Volume Tangki",
                      size: sp(16),
                      color: ColorUtils.grey4350,
                      fontWeight: FontType.bold,
                      fontFamily: FontFamily.gotham,
                    ),
                    RequestWrapperWidget<DataVolumeResponse>(
                      requestWrapper: _volumeWrapper,
                      placeholder: (_) => ListView.builder(
                        itemCount: 2,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (_, index) => ShimmerVolumeTangki(),
                      ),
                      builder: (_, data) => ListView.builder(
                        itemCount: data.data.layanan.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (_, index) => VolumeTangkiItem(data: data.data.layanan[index]),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        StyledText("Transaksi",
                          size: sp(16),
                          color: ColorUtils.grey4350,
                          fontWeight: FontType.bold,
                          fontFamily: FontFamily.gotham,
                        ),
                        Spacer(),
                        InkWell(
                          onTap: () => navigateTo(MyApp.ROUTE_TRANSAKSI),
                          child: StyledText("Detail",
                            size: sp(16),
                            color: ColorUtils.orange,
                            fontWeight: FontType.bold,
                            fontFamily: FontFamily.gotham,
                          )
                        ),
                      ]
                    ),
                    RequestWrapperWidget<DataPenjualanResponse>(
                      requestWrapper: _penjualanWrapper,
                      placeholder: (_) => ListView.builder(
                        itemCount: 2,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (_, __) => ShimmerHomeTransaksi(),
                      ),
                      builder: (_, data) => ListView.builder(
                        itemCount: data.data.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemBuilder: (_, index) => HomeTransaksiItem(data: data.data[index]),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
