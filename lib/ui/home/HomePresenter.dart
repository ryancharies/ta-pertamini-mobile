import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:pertamini/ancestor/BasePresenter.dart';
import 'package:pertamini/ancestor/BaseState.dart';
import 'package:pertamini/network/response/DataLayananResponse.dart';
import 'package:pertamini/network/response/DataPenjualanResponse.dart';
import 'package:pertamini/network/response/DataVolumeResponse.dart';
import 'package:pertamini/network/response/LoginResponse.dart';
import 'package:pertamini/preferences/AppPreferences.dart';
import 'package:pertamini/ui/home/HomeDelegate.dart';

class HomePresenter extends BasePresenter {
  static const REQUEST_GET_VOLUME = 0;
  static const REQUEST_GET_LAYANAN = 1;
  static const REQUEST_GET_PENJUALAN = 2;
  static const REQUEST_TOPUP = 3;

  final HomeDelegate _delegate;
  HomePresenter(BaseState state, this._delegate) : super(state);

  void executeGetVolume(RequestWrapper<DataVolumeResponse> wrapper,
   RequestWrapper<UserData> userWrapper) async {
    var user = await AppPreference.getUser();

    userWrapper.doRequest();
    wrapper.doRequest();
    api.executeGetVolume(REQUEST_GET_VOLUME, user.rfid, (response) async {
      user.epay = response.data.epay;
      await AppPreference.saveUser(user);
      
      userWrapper.finishRequest(user);
      wrapper.finishRequest(response);
    });
  }

  void executeGetLayanan() async{
    var user = await AppPreference.getUser();
    api.executeGetLayanan(REQUEST_GET_LAYANAN, user.id, _delegate.onSuccessGetLayanan);
  }

  void executeGetPenjualan(RequestWrapper<DataPenjualanResponse> wrapper) async {
    var user = await AppPreference.getUser();

    wrapper.doRequest();
    api.executeGetPenjualan(REQUEST_GET_PENJUALAN, user.rfid, wrapper.finishRequest);
  }

  void executeGetUser(RequestWrapper<UserData> wrapper) async {
    wrapper.doRequest();
    wrapper.finishRequest(await AppPreference.getUser());
  }

  void executeTopup(String code) async {
    var user = await AppPreference.getUser();
    var params = {
      "id" : user.id,
      "voucher" : code,
    };
    api.executeTopup(REQUEST_TOPUP, params, _delegate.onSuccessTopup);
  }
}