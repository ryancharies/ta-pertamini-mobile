import 'package:flutter/material.dart';
import 'package:pertamini/custom/view/text/StyledText.dart';
import 'package:pertamini/extension/Size.dart';
import 'package:pertamini/network/response/DataVolumeResponse.dart';
import 'package:pertamini/utils/ColorUtils.dart';
import 'package:pertamini/utils/FontUtils.dart';
import 'package:pertamini/utils/ImageUtils.dart';
import 'package:shimmer/shimmer.dart';

class VolumeTangkiItem extends StatelessWidget {
  final LayananData data;

  VolumeTangkiItem({@required this.data});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Image.asset(ImageUtils.ic_pom_blue,
              width: adaptiveWidth(context, 75),
              height: adaptiveWidth(context, 75),
              fit: BoxFit.fill,
            ),
            SizedBox(width: adaptiveWidth(context, 30)),
            Expanded(
              flex: 60,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  StyledText(data.layanan,
                    size: sp(context, 14),
                    color: ColorUtils.grey4350,
                    fontWeight: FontType.bold,
                    fontFamily: FontFamily.gotham,
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 40,
              child: StyledText("${data.volumeTangki}L",
                size: sp(context, 18),
                textAlign: TextAlign.end,
                color: ColorUtils.redLitre,
                fontFamily: FontFamily.gotham,
                fontWeight: FontType.bold,
              ),
            )
          ],
        ),
        SizedBox(height: adaptiveWidth(context, 20)),
      ],
    );
  }
}

class ShimmerVolumeTangki extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Shimmer.fromColors(
              child: Container(
                width: adaptiveWidth(context, 75),
                height: adaptiveWidth(context, 75),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.grey[200],
                ),
              ),
              baseColor: Colors.grey[200],
              highlightColor: Colors.white,
            ),
            SizedBox(width: adaptiveWidth(context, 30)),
            Shimmer.fromColors(
              child: Container(
                width: adaptiveWidth(context, 150),
                height: sp(context, 14),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.grey[200],
                ),
              ),
              baseColor: Colors.grey[200],
              highlightColor: Colors.white,
            ),
            Spacer(),
            Shimmer.fromColors(
              child: Container(
                width: adaptiveWidth(context, 50),
                height: sp(context, 18),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.grey[200],
                ),
              ),
              baseColor: Colors.grey[200],
              highlightColor: Colors.white,
            ),
          ],
        ),
        SizedBox(height: adaptiveWidth(context, 20)),
      ],
    );
  }
}
