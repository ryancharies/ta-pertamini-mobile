import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pertamini/custom/view/text/StyledText.dart';
import 'package:pertamini/extension/Size.dart';
import 'package:pertamini/network/response/DataPenjualanResponse.dart';
import 'package:pertamini/utils/ColorUtils.dart';
import 'package:pertamini/utils/FontUtils.dart';
import 'package:pertamini/utils/ImageUtils.dart';
import 'package:shimmer/shimmer.dart';
import 'package:intl/intl.dart';

class HomeTransaksiItem extends StatelessWidget {
  final PenjualanData data;
  final Function(PenjualanData) onSelected;

  HomeTransaksiItem({@required this.data, this.onSelected});

  @override
  Widget build(BuildContext context) {
    var now = DateTime.now();
    var tanggal = DateFormat("yyyy-MM-dd HH:mm:ss").parse(data.tanggal);
    var diff = now.difference(tanggal).inDays;
    var status = (data.status == 'diterima' && diff > 0) ? 'expired' : data.status;

    return InkWell(
      onTap: onSelected != null ? () => onSelected(data) : null,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Image.asset(ImageUtils.ic_pom_white,
                width: adaptiveWidth(context, 75),
                height: adaptiveWidth(context, 75),
                fit: BoxFit.fill,
              ),
              SizedBox(width: adaptiveWidth(context, 30)),
              Expanded(
                flex: 60,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    StyledText(data.kode,
                      size: sp(context, 14),
                      color: ColorUtils.grey4350,
                      fontWeight: FontType.bold,
                      fontFamily: FontFamily.gotham,
                    ),
                    SizedBox(height: adaptiveWidth(context, 10)),
                    StyledText(data.tanggal,
                      size: sp(context, 14),
                      color: ColorUtils.grey4350,
                      fontWeight: FontType.regular,
                      fontFamily: FontFamily.gotham,
                      fontStyle: FontStyle.italic,
                    ),
                    SizedBox(height: adaptiveWidth(context, 10)),
                    StyledText(status,
                      size: sp(context, 14),
                      color: ColorUtils.blue2399,
                      fontWeight: FontType.bold,
                      fontFamily: FontFamily.gotham,
                      fontStyle: FontStyle.normal,
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 40,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    StyledText("${data.totalLiter}L",
                      size: sp(context, 20),
                      color: ColorUtils.greenLitre,
                      fontFamily: FontFamily.gotham,
                      fontWeight: FontType.bold,
                    ),
                    SizedBox(height: adaptiveWidth(context, 10)),
                    StyledText("Rp. ${data.totalBayar}",
                      size: sp(context, 14),
                      color: ColorUtils.grey4350,
                      fontWeight: FontType.regular,
                      fontFamily: FontFamily.gotham,
                      fontStyle: FontStyle.italic,
                    ),
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: adaptiveWidth(context, 20)),
        ],
      ),
    );
  }
}

class ShimmerHomeTransaksi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Shimmer.fromColors(
              child: Container(
                width: adaptiveWidth(context, 75),
                height: adaptiveWidth(context, 75),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.grey[200],
                ),
              ),
              baseColor: Colors.grey[200],
              highlightColor: Colors.white,
            ),
            SizedBox(width: adaptiveWidth(context, 30)),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Shimmer.fromColors(
                  child: Container(
                    width: adaptiveWidth(context, 100),
                    height: sp(context, 14),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.grey[200],
                    ),
                  ),
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.white,
                ),
                SizedBox(height: adaptiveWidth(context, 10)),
                Shimmer.fromColors(
                  child: Container(
                    width: adaptiveWidth(context, 130),
                    height: sp(context, 14),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.grey[200],
                    ),
                  ),
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.white,
                ),
              ],
            ),
            Spacer(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Shimmer.fromColors(
                  child: Container(
                    width: adaptiveWidth(context, 50),
                    height: sp(context, 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.grey[200],
                    ),
                  ),
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.white,
                ),
                SizedBox(height: adaptiveWidth(context, 10)),
                Shimmer.fromColors(
                  child: Container(
                    width: adaptiveWidth(context, 75),
                    height: sp(context, 14),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.grey[200],
                    ),
                  ),
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.white,
                ),
              ],
            ),
          ],
        ),
        SizedBox(height: adaptiveWidth(context, 20)),
      ],
    );
  }
}

