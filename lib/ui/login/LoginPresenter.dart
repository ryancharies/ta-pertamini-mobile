import 'package:fluttertoast/fluttertoast.dart';
import 'package:pertamini/ancestor/BasePresenter.dart';
import 'package:pertamini/ancestor/BaseState.dart';
import 'package:pertamini/ui/login/LoginDelegate.dart';

class LoginPresenter extends BasePresenter {
  static const REQUEST_LOGIN = 0;

  final LoginDelegate _delegate;

  LoginPresenter(BaseState state, this._delegate) : super(state);

  void executeLogin(String rfid, String password){
    if(rfid == ""){
      Fluttertoast.showToast(msg: "RFID wajib diisi");
      return;
    }

    if(password == ""){
      Fluttertoast.showToast(msg: "Password wajib diisi");
      return;
    }

    api.executeLogin(REQUEST_LOGIN, {"rfid" : rfid, "password" : password}, _delegate.onSuccessLogin);
  }
}