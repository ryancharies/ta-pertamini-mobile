import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pertamini/ancestor/BaseState.dart';
import 'package:pertamini/custom/view/text/StyledText.dart';
import 'package:pertamini/extension/BaseStateExt.dart';
import 'package:pertamini/main.dart';
import 'package:pertamini/network/response/LoginResponse.dart';
import 'package:pertamini/preferences/AppPreferences.dart';
import 'package:pertamini/ui/login/LoginDelegate.dart';
import 'package:pertamini/ui/login/LoginPresenter.dart';
import 'package:pertamini/utils/ColorUtils.dart';
import 'package:pertamini/utils/FontUtils.dart';
import 'package:pertamini/utils/ImageUtils.dart';

class LoginUI extends StatefulWidget {
  @override
  _LoginUIState createState() => _LoginUIState();
}

class _LoginUIState extends BaseState<LoginUI> implements LoginDelegate{
  LoginPresenter _presenter;

  String _rfid = "";
  String _password = "";

  FocusNode _rfidFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    _presenter = LoginPresenter(this, this);
  }

  @override
  void onSuccessLogin(LoginResponse response) async {
    await AppPreference.saveUser(response.data);
    navigateTo(MyApp.ROUTE_HOME, singleTop: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: width(20)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.asset(ImageUtils.login_vector,
                width: double.infinity,
                height: width(250),
                fit: BoxFit.cover
              ),
              SizedBox(height: width(20)),
              StyledText("Selamat Datang!",
                size: sp(20),
                fontFamily: FontFamily.roboto,
                fontWeight: FontType.bold,
              ),
              SizedBox(height: width(40)),
              StyledText("ID Pelanggan",
                size: sp(12),
                fontFamily: FontFamily.roboto,
                fontWeight: FontType.bold,
                color: ColorUtils.grey1616.withOpacity(0.41),
              ),
              SizedBox(height: width(5)),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: ColorUtils.greyf2f2,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: TextFormField(
                  onChanged: (it) => _rfid = it,
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.done,
                  focusNode: _rfidFocus,
                  onFieldSubmitted: (_){
                    _rfidFocus.unfocus();
                    _passwordFocus.requestFocus();
                  },
                  style: TextStyle(
                    fontFamily: FontFamily.roboto,
                    fontSize: sp(14),
                  ),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: TextStyle(
                      color: Colors.black.withOpacity(0.41),
                      fontFamily: FontFamily.roboto,
                      fontSize: sp(14),
                    ),
                    hintText: "1230 2399 8749 1282",
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: width(30),
                      vertical: width(10),
                    ),
                  ),
                ),
              ),
              SizedBox(height: width(10)),
              StyledText("Password",
                size: sp(12),
                fontFamily: FontFamily.roboto,
                fontWeight: FontType.bold,
                color: ColorUtils.grey1616.withOpacity(0.41),
              ),
              SizedBox(height: width(5)),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: ColorUtils.greyf2f2,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: TextFormField(
                  obscureText: true,
                  onChanged: (it) => _password = it,
                  keyboardType: TextInputType.visiblePassword,
                  textInputAction: TextInputAction.done,
                  focusNode: _passwordFocus,
                  onFieldSubmitted: (_) => _passwordFocus.unfocus(),
                  style: TextStyle(
                    fontFamily: FontFamily.roboto,
                    fontSize: sp(14),
                  ),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: width(30),
                      vertical: width(10),
                    ),
                  ),
                ),
              ),
              SizedBox(height: width(70)),
              MaterialButton(
                onPressed: () => _presenter.executeLogin(_rfid, _password),
                color: ColorUtils.bgEpayGradientStart,
                minWidth: double.infinity,
                height: sp(45),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(sp(10)),
                ),
                child: StyledText("LOGIN",
                  fontFamily: FontFamily.roboto,
                  size: sp(16),
                  color: Colors.white,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
