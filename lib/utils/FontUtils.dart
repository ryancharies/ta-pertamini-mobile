import 'package:flutter/cupertino.dart';

class FontFamily {
  static const gotham = "Gotham";
  static const roboto = "Roboto";
}

class FontType {
  static const thin = FontWeight.w200;
  static const light = FontWeight.w300;
  static const regular = FontWeight.normal;
  static const semibold = FontWeight.w600;
  static const bold = FontWeight.bold;
  static const extrabold = FontWeight.w800;
  static const superbold = FontWeight.w900;
}