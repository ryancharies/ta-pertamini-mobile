class ImageUtils {
  static const ic_pom_blue = "assets/images/ic_pom_blue.png";
  static const ic_pom_white = "assets/images/ic_pom_white.png";
  static const fuel = "assets/images/fuel.png";
  static const login_vector = "assets/images/login_vector.png";
}