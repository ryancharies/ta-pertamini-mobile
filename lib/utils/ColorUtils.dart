import 'dart:ui';

class ColorUtils {
  static const bgColor = Color(0xfffbfcfc);

  static const bgEpayGradientStart = Color(0xff26199a);
  static const bgEpayGradientEnd = Color(0xff2e2799);

  static const blue2399 = Color(0xff2c2399);

  static const grey4350 = Color(0xff464350);
  static const greyc0c0 = Color(0xffc1c0c0);
  static const grey9f9f = Color(0xff9f9f9f);
  static const grey8888 = Color(0xff888888);
  static const grey1616 = Color(0xff161616);
  static const greyde000000 = Color(0xde000000);
  static const greybebe = Color(0xffbebebe);
  static const greyf2f2 = Color(0xfff2f2f2);

  static const orange = Color(0xffffbf45);

  static const redLitre = Color(0xffff454b);
  static const greenLitre = Color(0xff3fe894);
}