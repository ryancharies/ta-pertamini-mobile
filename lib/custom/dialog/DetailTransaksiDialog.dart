import 'package:flutter/material.dart';
import 'package:pertamini/custom/view/text/StyledText.dart';
import 'package:pertamini/extension/Size.dart';
import 'package:pertamini/network/response/DataPenjualanResponse.dart';
import 'package:pertamini/utils/ColorUtils.dart';
import 'package:pertamini/utils/FontUtils.dart';

class DetailTransaksiDialog extends StatelessWidget {
  final PenjualanData data;

  DetailTransaksiDialog({@required this.data});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(adaptiveWidth(context, 25)),
          topLeft: Radius.circular(adaptiveWidth(context, 25)),
        )
      ),
      padding: EdgeInsets.symmetric(horizontal: adaptiveWidth(context, 15)),
      child: Wrap(
        children: <Widget>[
          Column(
            children: <Widget>[
              SizedBox(height: adaptiveWidth(context, 20)),
              Container(
                width: adaptiveWidth(context, 75),
                color: ColorUtils.greybebe,
                height: 5,
              ),
              SizedBox(height: adaptiveWidth(context, 30)),
              StyledText(data.kode,
                size: sp(context, 14),
                fontFamily: FontFamily.gotham,
                fontWeight: FontType.bold,
              ),
              StyledText(data.tanggal,
                size: sp(context, 12),
                fontFamily: FontFamily.gotham,
                color: ColorUtils.grey8888,
                fontStyle: FontStyle.italic,
              ),
              SizedBox(height: adaptiveWidth(context, 40)),
              ListView.builder(
                itemCount: data.detail.length,
                itemBuilder: (_, index) => DetailItem(detail: data.detail[index]),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
              ),
              SizedBox(height: adaptiveWidth(context, 5)),
              Container(
                width: adaptiveWidth(context, 330),
                color: ColorUtils.greybebe,
                height: 1,
              ),
              SizedBox(height: adaptiveWidth(context, 20)),
              Row(
                children: <Widget>[
                  StyledText("Total Bayar",
                    size: sp(context, 14),
                    fontFamily: FontFamily.gotham,
                    fontWeight: FontType.superbold,
                  ),
                  Spacer(),
                  StyledText("Rp ${data.totalBayar}",
                    size: sp(context, 12),
                    fontFamily: FontFamily.gotham,
                  ),
                ],
              ),
              SizedBox(height: adaptiveWidth(context, 15)),
              Row(
                children: <Widget>[
                  StyledText("Total Liter",
                    size: sp(context, 14),
                    fontFamily: FontFamily.gotham,
                    fontWeight: FontType.superbold,
                  ),
                  Spacer(),
                  StyledText("Rp ${data.totalLiter}",
                    size: sp(context, 12),
                    fontFamily: FontFamily.gotham,
                  ),
                ],
              ),
              SizedBox(height: adaptiveWidth(context, 15)),
            ],
          )
        ],
      ),
    );
  }
}

class DetailItem extends StatelessWidget {
  final PenjualanDetail detail;

  DetailItem({@required this.detail});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            StyledText(detail.keterangan,
              size: sp(context, 14),
              fontFamily: FontFamily.gotham,
              fontWeight: FontType.superbold,
            ),
            Spacer(),
            StyledText("Rp ${detail.bayar} | ${detail.liter} L ",
              size: sp(context, 12),
              fontFamily: FontFamily.gotham,
            ),
          ],
        ),
        SizedBox(height: adaptiveWidth(context, 15)),
      ],
    );
  }
}
