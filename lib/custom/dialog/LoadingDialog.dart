import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingDialog extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: SpinKitCircle(
        color: Colors.white,
      ),
      onWillPop: () async {
        return false;
      },
    );
  }
}