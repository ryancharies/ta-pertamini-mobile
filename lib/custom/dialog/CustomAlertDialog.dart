import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pertamini/custom/view/text/StyledText.dart';
import 'package:pertamini/extension/Size.dart';
import 'package:pertamini/utils/ColorUtils.dart';
import 'package:pertamini/utils/FontUtils.dart';

class CustomAlertDialog extends StatelessWidget {
  final String title;
  final String msg;
  final String negativeTitle;
  final String positiveTitle;

  CustomAlertDialog({@required this.title, @required this.msg, this.negativeTitle = "Tidak", this.positiveTitle = "Ya"});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(adaptiveWidth(context, 15)),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(adaptiveWidth(context, 20)),
            decoration: BoxDecoration(
              color: ColorUtils.bgEpayGradientStart,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(adaptiveWidth(context, 15)),
                topRight: Radius.circular(adaptiveWidth(context, 15)),
              ),
            ),
            alignment: Alignment.center,
            child: StyledText(title,
              fontFamily: FontFamily.gotham,
              fontWeight: FontType.superbold,
              color: Colors.white,
              size: sp(context, 14),
            ),
          ),
          Container(
            padding: EdgeInsets.all(adaptiveWidth(context, 20)),
            child: StyledText(msg,
              fontFamily: FontFamily.gotham,
              fontWeight: FontType.superbold,
              size: sp(context, 12),
              textAlign: TextAlign.center,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: StyledText(negativeTitle,
                  fontFamily: FontFamily.gotham,
                  fontWeight: FontType.superbold,
                  size: sp(context, 12),
                  textAlign: TextAlign.center,
                ),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: StyledText(positiveTitle,
                  fontFamily: FontFamily.gotham,
                  fontWeight: FontType.superbold,
                  size: sp(context, 12),
                  color: ColorUtils.bgEpayGradientStart,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(width: adaptiveWidth(context, 10)),
            ],
          )
        ],
      ),
    );
  }
}
