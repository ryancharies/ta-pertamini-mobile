import 'package:flutter/material.dart';
import 'package:pertamini/custom/view/text/StyledText.dart';
import 'package:pertamini/extension/Size.dart';
import 'package:pertamini/utils/ColorUtils.dart';
import 'package:pertamini/utils/FontUtils.dart';

class TopupDialog extends StatelessWidget {

  String _code = "";

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(adaptiveWidth(context, 15)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(adaptiveWidth(context, 20)),
            decoration: BoxDecoration(
              color: ColorUtils.bgEpayGradientStart,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(adaptiveWidth(context, 15)),
                topRight: Radius.circular(adaptiveWidth(context, 15)),
              ),
            ),
            alignment: Alignment.center,
            child: StyledText("Topup",
              fontFamily: FontFamily.gotham,
              fontWeight: FontType.superbold,
              color: Colors.white,
              size: sp(context, 14),
            ),
          ),
          Container(
            padding: EdgeInsets.all(adaptiveWidth(context, 20)),
            child: StyledText("Masukkan kode voucher",
              fontFamily: FontFamily.gotham,
              fontWeight: FontType.superbold,
              size: sp(context, 12),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: adaptiveWidth(context, 20)),
            child: TextFormField(
              style: TextStyle(
                fontFamily: FontFamily.gotham,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
              onChanged: (it) => _code = it,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: StyledText("BATAL",
                  fontFamily: FontFamily.gotham,
                  fontWeight: FontType.superbold,
                  size: sp(context, 12),
                  textAlign: TextAlign.center,
                ),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(_code),
                child: StyledText("KIRIM",
                  fontFamily: FontFamily.gotham,
                  fontWeight: FontType.superbold,
                  size: sp(context, 12),
                  color: ColorUtils.bgEpayGradientStart,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(width: adaptiveWidth(context, 10)),
            ],
          )
        ],
      ),
    );
  }
}
